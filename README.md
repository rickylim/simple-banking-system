# Simple banking system

Simple banking system is a command-line interface for simple bank account.
The main feature is to manage bank accounts, and money transfer between accounts.

# Learning outcomes

- SQL API
- Exception API
- Stream API

# Build 

```bash
./gradlew clean jar 
```

The `jar` is available `build/libs/simple-banking-system-1.0.jar`

## Demo

```bash
java -jar build/libs/simple-banking-system-1.0.jar -fileName bank.db
```