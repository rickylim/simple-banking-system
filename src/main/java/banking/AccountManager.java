package banking;

import banking.card.Card;
import banking.card.CardDB;
import banking.card.CardNotFoundException;
import org.sqlite.SQLiteDataSource;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

import static banking.card.cardutil.CardGenerator.generateCardNumber;
import static banking.card.cardutil.CardGenerator.generatePin;
import static banking.card.cardutil.CardValidity.isCardNumberValid;

public class AccountManager {

    private final Scanner scanner;
    private final CardDB cardDB;


    public AccountManager(Scanner scanner, String fileName) {
        this.scanner = scanner;
        String url = "jdbc:sqlite:" + fileName;

        Connection connection = createConnection(url);
        cardDB = new CardDB(connection);
        cardDB.create();
    }


    public Connection createConnection(String dbUrl) {
        SQLiteDataSource dataSource = new SQLiteDataSource();
        dataSource.setUrl(dbUrl);

        try {
            return dataSource.getConnection();
        } catch (SQLException e) {
            throw new RuntimeException(String.format("Get connection failed: %s", e.getMessage()));
        }
    }


    private boolean isExistingCard(String cardNumber) {
        try {
            cardDB.findByCardNumber(cardNumber);
            return true;
        } catch (CardNotFoundException e) {
            return false;
        }
    }


    private Card getAccountCard(String cardNumber) {
        try {
            return cardDB.findByCardNumber(cardNumber);
        } catch (CardNotFoundException e) {
            throw new RuntimeException(String.format("Card: %s, NOT FOUND", cardNumber));
        }
    }


    private boolean canLogin(Card loginCard) {
        try {
            var accountCard = cardDB.findByCardNumber(loginCard.getNumber());
            return accountCard.getPin().equals(loginCard.getPin());
        } catch (CardNotFoundException e) {
            return false;
        }
    }


    public void readMainMenu() {
        System.out.println("1. Create an account");
        System.out.println("2. Log into account");
        System.out.println("0. Exit");

        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                createNewAccount();
                break;
            case 2:
                logIntoAccount();
                break;
            case 0:
                System.out.println();
                System.out.println("Bye!");
                System.exit(0);
            default:
                throw new IllegalArgumentException(String.format("Choice: %s, not found", choice));
        }

        readMainMenu();

    }


    private void createNewAccount() {
        String newCardNumber = generateCardNumber();
        if (isExistingCard(newCardNumber)) {
            createNewAccount();
        }

        String newPin = generatePin();
        var newCard = new Card(newCardNumber, newPin);
        cardDB.add(newCard);

        System.out.println();
        System.out.println("Your card has been created");
        System.out.println("Your card number:");
        System.out.println(newCard.getNumber());
        System.out.println("Your card PIN:");
        System.out.println(newCard.getPin());
        System.out.println();
    }


    private void logIntoAccount() {
        System.out.println();
        System.out.println("Enter your card number:");
        String inputCardNumber = scanner.next();
        System.out.println("Enter your PIN:");
        String inputPin = scanner.next();
        var loginCard = new Card(inputCardNumber, inputPin);

        if (!canLogin(loginCard)) {
            System.out.println();
            System.out.println("Wrong card number or PIN!");
            System.out.println();
            readMainMenu();
        }

        var accountCard = getAccountCard(loginCard.getNumber());
        System.out.println();
        System.out.println("You have successfully logged in!");
        System.out.println();
        readAccountMenu(accountCard);

    }


    public void readAccountMenu(Card loginCard) {
        System.out.println("1. Balance");
        System.out.println("2. Add income");
        System.out.println("3. Do transfer");
        System.out.println("4. Close account");
        System.out.println("5. Log out");
        System.out.println("0. Exit");

        int choice = scanner.nextInt();
        switch (choice) {
            case 1:
                System.out.println();
                printBalance(loginCard);
                System.out.println();
                readAccountMenu(loginCard);
                break;
            case 2:
                System.out.println();
                addIncomeMenu(loginCard);
                System.out.println();
                readAccountMenu(loginCard);
            case 3:
                System.out.println();
                transferMenu(loginCard);
                System.out.println();
                readAccountMenu(loginCard);
            case 4:
                System.out.println();
                closeAccountMenu(loginCard);
                System.out.println();
                readMainMenu();
            case 5:
                System.out.println();
                System.out.println("You have successfully logged out");
                System.out.println();
                readMainMenu();
                break;
            case 0:
                System.out.println();
                System.out.println("Bye!");
                System.exit(0);
            default:
                throw new IllegalArgumentException(String.format("Choice: %s, not found", choice));
        }
    }


    public void printBalance(Card loginCard) {
        try {
            var card = cardDB.findByCardNumber(loginCard.getNumber());
            System.out.printf("Balance: %s%n", card.getBalance().toPlainString());
            System.out.println();
        } catch (CardNotFoundException e) {
            throw new RuntimeException(
                    String.format("Card: %s, NOT FOUND. Error message: %s.", loginCard.getNumber(), e.getMessage())
            );
        }
    }


    public void addIncomeMenu(Card loginCard) {
        System.out.println("Enter income:");
        BigDecimal amount = scanner.nextBigDecimal();

        var newBalance = loginCard.getBalance().add(amount);
        var newCard = new Card(loginCard.getNumber(), loginCard.getPin(), newBalance);

        cardDB.update(newCard);
        System.out.println("Income was added!");
    }


    public void transferMenu(Card loginCard) {
        System.out.println("Transfer");
        System.out.println("Enter card number");
        var toCardNumber = scanner.next();

        if (!isCardNumberValid(toCardNumber)) {
            System.out.println("Probably you made a mistake in the card number. Please try again!");
            System.out.println();
            readAccountMenu(loginCard);
        }

        if (!isExistingCard(toCardNumber)) {
            System.out.println("Such a card does not exist.");
            System.out.println();
            readAccountMenu(loginCard);
        }

        if (loginCard.getNumber().equals(toCardNumber)) {
            System.out.println("You can't transfer money to the same account!");
            System.out.println();
            readAccountMenu(loginCard);
        }

        System.out.println("Enter how much money you want to transfer:");
        var transferAmount = scanner.nextBigDecimal();

        var fromCard = getAccountCard(loginCard.getNumber());
        if (fromCard.getBalance().compareTo(transferAmount) < 0) {
            System.out.println("Not enough money!");
            System.out.println();
            readAccountMenu(loginCard);
        }

        var fromNewBalance = fromCard.getBalance().subtract(transferAmount);
        var fromNewCard = new Card(fromCard.getNumber(), fromCard.getPin(), fromNewBalance);

        var toCard = getAccountCard(toCardNumber);
        var toNewBalance = toCard.getBalance().add(transferAmount);
        var toNewCard = new Card(toCard.getNumber(), toCard.getPin(), toNewBalance);

        cardDB.transfer(fromNewCard, toNewCard);
        System.out.println("Success");

    }


    public void closeAccountMenu(Card loginCard) {
        var accountCard = getAccountCard(loginCard.getNumber());
        cardDB.delete(accountCard);
        System.out.println("The account has been closed");
    }


}
