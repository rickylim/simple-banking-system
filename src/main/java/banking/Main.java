package banking;

import java.util.Scanner;

public class Main {
    private static final String FILENAME_ARG = "-fileName";


    public static void main(String[] args) {
        String fileName = parseFileName(args);
        var scanner = new Scanner(System.in);
        try (scanner){
            var accountManager = new AccountManager(scanner, fileName);
            accountManager.readMainMenu();
        }
    }


    private static String parseFileName(String[] args) {
        String fileName = null;
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals(FILENAME_ARG)) {
                fileName = args[i + 1];
                break;
            }
        }

        if (fileName == null) {
            throw new IllegalArgumentException(String.format("arg: %s is required", FILENAME_ARG));
        }

        return fileName;
    }


}