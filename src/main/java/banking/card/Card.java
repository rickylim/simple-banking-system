package banking.card;

import java.math.BigDecimal;

public class Card {
    private final String number;
    private final String pin;
    private final BigDecimal balance;

    public String getNumber() {
        return number;
    }

    public String getPin() {
        return pin;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Card(String card, String pin) {
        this.number = card;
        this.pin = pin;
        this.balance = BigDecimal.ZERO;
    }

    public Card(String number, String pin, BigDecimal balance) {
        this.number = number;
        this.pin = pin;
        this.balance = balance;
    }


    @Override
    public String toString() {
        return "Card{" +
                "number='" + number + '\'' +
                ", pin='" + pin + '\'' +
                ", balance=" + balance +
                '}';
    }

}
