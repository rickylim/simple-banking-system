package banking.card;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CardDB {
    private static final String CREATE = "CREATE TABLE IF NOT EXISTS card (" +
            "id INTEGER PRIMARY KEY, " +
            "number TEXT NOT NULL UNIQUE, " +
            "pin TEXT NOT NULL, " +
            "balance INTEGER DEFAULT 0);";
    private static final String INSERT = "INSERT into card(number, pin) " +
            "VALUES (?, ?);";
    private static final String GET_ONE = "SELECT number, pin, balance " +
            "FROM card " +
            "WHERE number = ?;";
    private static final String UPDATE = "UPDATE card " +
            "SET balance = ? " +
            "WHERE number = ? AND pin = ?;";
    private static final String DELETE = "DELETE FROM card " +
            "WHERE number = ? AND pin = ?;";

    private final Connection connection;


    public CardDB(Connection connection) {
        this.connection = connection;
    }


    public void create() {
        try (PreparedStatement statement = this.connection.prepareStatement(CREATE)) {
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(String.format("Create table failed: %s", e.getMessage()));
        }
    }


    public void add(Card card) {
        try (PreparedStatement statement = this.connection.prepareStatement(INSERT)) {
            statement.setString(1, card.getNumber());
            statement.setString(2, card.getPin());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(String.format("Add card: %s FAILED: %s", card, e.getMessage()));
        }
    }


    public void delete(Card card) {
        try (PreparedStatement statement = this.connection.prepareStatement(DELETE)) {
            statement.setString(1, card.getNumber());
            statement.setString(2, card.getPin());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(String.format("Add card: %s FAILED: %s", card, e.getMessage()));
        }
    }


    public Card findByCardNumber(String cardNumber) throws CardNotFoundException {
        Card card = null;

        try (PreparedStatement statement = this.connection.prepareStatement(GET_ONE)) {
            statement.setString(1, cardNumber);
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                card = new Card(
                        rs.getString("number"),
                        rs.getString("pin"),
                        rs.getBigDecimal("balance")
                );
            }
        } catch (SQLException e) {
            throw new RuntimeException(String.format("Get card number: %s, FAILED: %s", cardNumber, e.getMessage()));
        }

        if (card == null) {
            throw new CardNotFoundException(String.format("Card with number: %s, NOT FOUND", cardNumber));
        }

        return card;
    }


    public void update(Card card) {
        try (PreparedStatement statement = this.connection.prepareStatement(UPDATE)) {
            statement.setInt(1, card.getBalance().intValue());
            statement.setString(2, card.getNumber());
            statement.setString(3, card.getPin());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(
                    String.format("Update balance of card number: %s, FAILED: %s", card.getNumber(), e.getMessage())
            );
        }
    }


    public void transfer(Card fromCard, Card toCard) {
        try {
            this.connection.setAutoCommit(false);

            try (PreparedStatement fromStatement = this.connection.prepareStatement(UPDATE);
                 PreparedStatement toStatement = this.connection.prepareStatement(UPDATE)) {

                fromStatement.setInt(1, fromCard.getBalance().intValue());
                fromStatement.setString(2, fromCard.getNumber());
                fromStatement.setString(3, fromCard.getPin());

                toStatement.setInt(1, toCard.getBalance().intValue());
                toStatement.setString(2, toCard.getNumber());
                toStatement.setString(3, toCard.getPin());

                fromStatement.executeUpdate();
                toStatement.executeUpdate();
                this.connection.commit();
                this.connection.setAutoCommit(true);
            } catch (SQLException e) {
                this.connection.rollback();
            }

        } catch (SQLException e) {
            throw new RuntimeException(
                    String.format("Transfer from %s to %s: FAILED. Error: %s",
                            fromCard.getNumber(), toCard.getNumber(), e.getMessage())
            );
        }
    }


}
