package banking.card.cardutil;

import java.util.Random;
import java.util.stream.IntStream;

public class CardGenerator {
    private static final int BANK_IDENTIFICATION_NUMBER = 400_000;
    private static final int ACCOUNT_NUMBER_SIZE = 9;
    private static final int UPPER_NUMBER = 10;
    private static final int PIN_NUMBER_SIZE = 4;

    private static final Random random = new Random();


    public static String generateCardNumber() {
        var cardNumber = new StringBuilder();
        cardNumber.append(BANK_IDENTIFICATION_NUMBER);
        IntStream.range(0, ACCOUNT_NUMBER_SIZE)
                .forEach(n -> cardNumber.append(random.nextInt(UPPER_NUMBER)));

        String checksum = generateCheckSum(cardNumber.toString());

        return cardNumber.append(checksum).toString();
    }


    public static String generatePin() {
        var pinNumber = new StringBuilder();
        IntStream.range(0, PIN_NUMBER_SIZE)
                .forEach(n -> pinNumber.append(random.nextInt(UPPER_NUMBER)));

        return pinNumber.toString();
    }


    public static String generateCheckSum(String numericString) {
        int controlNumber = IntStream.range(0, numericString.length())
                .mapToObj(i -> {
                    int number = Character.getNumericValue(numericString.charAt(i));
                    if (i % 2 == 0) {
                        number = number * 2;
                    }
                    return number;
                })
                .mapToInt(n -> n)
                .map(n -> n > 9 ? n - 9 : n)
                .reduce(0, Integer::sum);

        int checkSum = controlNumber % 10 != 0 ? Math.abs(10 - (controlNumber % 10)) : 0;

        return Integer.toString(checkSum);
    }


}
