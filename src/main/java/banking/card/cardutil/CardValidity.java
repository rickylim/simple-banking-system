package banking.card.cardutil;

import static banking.card.cardutil.CardGenerator.generateCheckSum;

public class CardValidity {


    public static boolean isCardNumberValid(String cardNumber) {
        var lastNumber = cardNumber.charAt(cardNumber.length() - 1);
        var accountNumbers = cardNumber.substring(0, cardNumber.length() - 1);

        var checkSum = generateCheckSum(accountNumbers);

        return Character.getNumericValue(lastNumber) == Integer.parseInt(checkSum);
    }


}
